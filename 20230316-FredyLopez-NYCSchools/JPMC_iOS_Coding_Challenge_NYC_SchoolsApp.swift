//
//  JPMC_iOS_Coding_Challenge_NYC_SchoolsApp.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/16/23.
//

import SwiftUI

@main
struct JPMC_iOS_Coding_Challenge_NYC_SchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView()
        }
    }
}
