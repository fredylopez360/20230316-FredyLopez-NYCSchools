//
//  Response.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/16/23.
//
//

import Foundation



struct SATScore: Decodable {
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}

extension SATScore: Identifiable {
    var id: String { dbn }
}


extension SATScore {
    
    static var mock: SATScore {
        SATScore(dbn: "0000",
                  school_name: "School",
                  num_of_sat_test_takers: "1,000",
                  sat_critical_reading_avg_score: "400",
                  sat_math_avg_score: "300",
                  sat_writing_avg_score: "200")
    }
}
