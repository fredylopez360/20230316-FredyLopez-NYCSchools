//
//  NetworkError.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/16/23.
//
// Used in NetworkManager.swift
//

import Foundation

enum NetworkError: Error {
    case badURL
    case boxed(Error)
}
