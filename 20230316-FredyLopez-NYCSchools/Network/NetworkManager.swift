//
//  NetworkManager.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/16/23.
//
// API URL: https://data.cityofnewyork.us/resource/f9bf-2cp4.json
//

import Foundation
import Alamofire


class NetworkManager: SessionDelegate {
    
    lazy var session: Session = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForRequest = 16.0
        return Session(configuration: configuration, delegate: self)
    }()
    
    
    func downloadSchools( completion: @escaping (Result<[SATScore], NetworkError>) -> Void) {
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(.failure(.badURL))
            }
            return
        }
        
        session
            .download(url)
            .responseDecodable(of: [SATScore].self) { a in
                switch a.result {
                case .success(let characters):
                    DispatchQueue.main.async {
                        completion(.success(characters))
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(.failure(.boxed(error)))
                    }
                }
            }
    }
    
}
