//
//  SchoolDetailView.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/17/23.
//


import SwiftUI

struct SchoolDetailView: View {
    let school: SATScore
    
    var body: some View {
        VStack {
            Text(school.school_name)
                .font(.title)
                .padding()
            Divider()
            VStack(alignment: .leading) {
                Text("Number of SAT Test Takers:")
                    .font(.headline)
                Text(school.num_of_sat_test_takers ?? "N/A")
                    .font(.subheadline)
            }
            .padding()
            Divider()
            VStack(alignment: .leading) {
                Text("SAT Critical Reading Average Score:")
                    .font(.headline)
                Text(school.sat_critical_reading_avg_score ?? "N/A")
                    .font(.subheadline)
            }
            .padding()
            Divider()
            VStack(alignment: .leading) {
                Text("SAT Math Average Score:")
                    .font(.headline)
                Text(school.sat_math_avg_score ?? "N/A")
                    .font(.subheadline)
            }
            .padding()
            Divider()
            VStack(alignment: .leading) {
                Text("SAT Writing Average Score:")
                    .font(.headline)
                Text(school.sat_writing_avg_score ?? "N/A")
                    .font(.subheadline)
            }
            .padding()
            Divider()
        }
    }
}


struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailView(school: SATScore.mock)
    }
}
