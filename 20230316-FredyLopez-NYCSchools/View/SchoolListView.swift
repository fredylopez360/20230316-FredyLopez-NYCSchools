//
//  ContentView.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/16/23.
//
// Data grabbed from the Network, Structured in the Model, Shown here in the View, from the data Polished in the ViewModel

import SwiftUI

struct SchoolListView: View {
    @ObservedObject var schoolsService = SchoolsService()
    
    var body: some View {
        NavigationView {
            List(schoolsService.schools) { school in
                NavigationLink(destination: SchoolDetailView(school: school)) {
                    Text(school.school_name)
                }
            }
            .navigationBarTitle("NYC Schools")
            .onAppear {
                schoolsService.onAppear()
            }
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView()
    }
}
