//
//  ViewModel.swift
//  20230316-FredyLopez-NYCSchools
//
//  Created by Fredy lopez on 3/16/23.
//
// Note: If I had more time I would add each schools Overview Paragraph, Address, Phone Number, ECT.
//

import Foundation
import SwiftUI

class SchoolsService: ObservableObject {
    private let networkManager = NetworkManager()
    
    @Published var schools: [SATScore] = []
    public func onAppear() {
        getSchools()
    }
    
    private func getSchools() {
        NetworkManager().downloadSchools { completion in
            switch completion {
            case .success(let schools):
                self.schools = schools
            case .failure(let error):
                print("error = \(error)")
            }
        }
    }
    
    
}
